<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Auth::routes();
Route::get('/scan', 'ArsipKeluarController@scan');
Route::middleware('auth')->group(function () {
    Route::get('logout', '\App\Http\Controllers\Auth\LoginController@logout');
    Route::get('/', 'HomeController@index');

    Route::get('/home', 'HomeController@index')->name('home');

    Route::middleware('role.admin')->group(function () {
        Route::prefix('bidang')->group(function () {
            Route::get('/', 'BidangController@index')->name('bidang.index');
            Route::get('/create', 'BidangController@create')->name('bidang.create');
            Route::post('/', 'BidangController@store')->name('bidang.store');
            Route::get('/edit/{bidang}', 'BidangController@edit')->name('bidang.edit');
            Route::post('/update/{bidang}', 'BidangController@update')->name('bidang.update');
            Route::delete('/destroy/{bidang}', 'BidangController@destroy')->name('bidang.destroy');
            Route::get('/detail/{bidang}', 'BidangController@show')->name('bidang.show');
        });
    });
    Route::middleware('role.admin')->group(function () {
        Route::prefix('kantor')->group(function () {
            Route::get('/', 'KantorController@index')->name('kantor.index');
            Route::get('/create', 'KantorController@create')->name('kantor.create');
            Route::post('/', 'KantorController@store')->name('kantor.store');
            Route::get('/edit/{id}', 'KantorController@edit')->name('kantor.edit');
            Route::post('/update/{id}', 'KantorController@update')->name('kantor.update');
            Route::delete('/destroy/{id}', 'KantorController@destroy')->name('kantor.destroy');
            Route::get('/detail/{id}', 'KantorController@show')->name('kantor.show');
        });
    });
    Route::middleware('role.admin')->group(function () {
        Route::prefix('role')->group(function () {
            Route::get('/', 'RoleController@index')->name('role.index');
            Route::get('/create', 'RoleController@create')->name('role.create');
            Route::post('/', 'RoleController@store')->name('role.store');
            Route::get('/edit/{id}', 'RoleController@edit')->name('role.edit');
            Route::post('/update/{id}', 'RoleController@update')->name('role.update');
            Route::delete('/destroy/{id}', 'RoleController@destroy')->name('role.destroy');
            Route::get('/detail/{id}', 'RoleController@show')->name('role.show');
        });
    });
    Route::middleware('role.admin')->group(function () {
        Route::prefix('user')->group(function () {
            Route::get('/', 'UserController@index')->name('user.index');
            Route::get('/create', 'UserController@create')->name('user.create');
            Route::post('/', 'UserController@store')->name('user.store');
            Route::get('/edit/{id}', 'UserController@edit')->name('user.edit');
            Route::post('/update/{id}', 'UserController@update')->name('user.update');
            Route::delete('/destroy/{id}', 'UserController@destroy')->name('user.destroy');
            Route::get('/detail/{id}', 'UserController@show')->name('user.show');
        });
    });
    Route::middleware('role.admin')->group(function () {
        Route::prefix('arsipmasuk')->group(function () {
            Route::get('/', 'ArsipMasukController@index')->name('arsipmasuk.index');
            Route::get('/create', 'ArsipMasukController@create')->name('arsipmasuk.create');
            Route::post('/', 'ArsipMasukController@store')->name('arsipmasuk.store');
            Route::get('/edit/{id}', 'ArsipMasukController@edit')->name('arsipmasuk.edit');
            Route::post('/update/{id}', 'ArsipMasukController@update')->name('arsipmasuk.update');
            Route::delete('/destroy/{id}', 'ArsipMasukController@destroy')->name('arsipmasuk.destroy');
            Route::get('/detail/{id}', 'ArsipMasukController@show')->name('arsipmasuk.show');
            Route::get('/cetakPDF/{id}', 'ArsipMasukController@cetakPDF')->name('arsipmasuk.pdf');
            Route::get('/document/{id}', 'ArsipMasukController@download')->name('arsipmasuk.document');
        });
    });
    //  Route::middleware('role.admin')->group(function() {
    Route::prefix('arsipkeluar')->group(function () {
        Route::get('/', 'ArsipKeluarController@index')->name('arsipkeluar.index');
        Route::get('/create', 'ArsipKeluarController@create')->name('arsipkeluar.create');
        Route::post('/', 'ArsipKeluarController@store')->name('arsipkeluar.store');
        Route::get('/edit/{id}', 'ArsipKeluarController@edit')->name('arsipkeluar.edit');
        Route::post('/update/{id}', 'ArsipKeluarController@update')->name('arsipkeluar.update');
        Route::delete('/destroy/{id}', 'ArsipKeluarController@destroy')->name('arsipkeluar.destroy');
        Route::get('/detail/{id}', 'ArsipKeluarController@show')->name('arsipkeluar.show');
        Route::get('/cetakPDF/{id}', 'ArsipKeluarController@cetakPDF')->name('arsipkeluar.pdf');
        Route::get('/document/{id}', 'ArsipKeluarController@download')->name('arsipkeluar.document');
    });
    // });

    //  Route::middleware('role.admin')->group(function() {
    Route::prefix('laporan')->group(function () {
        Route::prefix('arsipmasuk')->group(function () {
            Route::get('/', 'LaporanController@arsipmasuk')->name('laporan.arsipmasuk');
            Route::get('/export_excel', 'LaporanController@arsipmasuk_excel')->name('laporan.arsipmasuk.excel');
            Route::get('/export_pdf', 'LaporanController@arsipmasuk_pdf')->name('laporan.arsipmasuk.pdf');
        });
        Route::prefix('arsipkeluar')->group(function () {
            Route::get('/', 'LaporanController@arsipkeluar')->name('laporan.arsipkeluar');
            Route::get('/export_excel', 'LaporanController@arsipkeluar_excel')->name('laporan.arsipkeluar.excel');
            Route::get('/export_pdf', 'LaporanController@arsipkeluar_pdf')->name('laporan.arsipkeluar.pdf');
        });
    });
    // });
});
