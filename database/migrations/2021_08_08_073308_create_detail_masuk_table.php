<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDetailMasukTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('detail_masuk', function (Blueprint $table) {
            $table->uuid('id')->primary();
            $table->uuid('id_masuk');
            $table->string('bidang');
            $table->timestamps();
        });
        Schema::table('detail_masuk', function (Blueprint  $table) {
            $table->foreign('id_masuk')->references('id')->on('arsip_masuk')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('detail_masuk');
    }
}
