<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateArsipMasukTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('arsip_masuk', function (Blueprint $table) {
            $table->uuid('id')->primary();
            $table->string('pengirim');
            $table->date('tgl_surat');
            $table->date('tgl_terima');
            // $table->integer('no_agenda');
            $table->string('no_surat')->nullable();
            $table->string('sifat')->nullable();
            $table->string('klasifikasi')->nullable();
            $table->string('keterangan')->nullable();
            $table->longText('perihal');
            $table->longText('isi')->nullable();
            $table->longText('file');
            $table->longText('status')->nullable();
            $table->string('bidang');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('arsip_masuk');
    }
}
