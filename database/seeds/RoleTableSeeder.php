<?php

use App\Role;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class RoleTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Role::create([
            'id'      => '1',
            'nama'      => 'admin',
        ]);
        Role::create([
            'id'      => '2',
            'nama'      => 'pegawai',
        ]);
    }
}
