@extends('admin.layouts.app')
@section('content')
<!-- Begin Page Content -->
<div class="container-fluid">

    <div class="card shadow mb-4">
        <div class="card-header py-3">
            <h6 class="m-0 font-weight-bold text-primary">Tambah user</h6>
        </div>
        <div class="card-body">
            <form action="{{ route('user.store') }}" method="post" enctype="multipart/form-data">
                @csrf
                <div class="form-group row">
                    <label for="example-text-input" class="col-sm-2 col-form-label">Nama user</label>
                    <div class="col-sm-10">
                        <input class="form-control" type="text" placeholder="Masukkan nama" name='nama'
                            id="example-text-input">
                    </div>
                </div>
                <div class="form-group row">
                    <label for="example-text-input" class="col-sm-2 col-form-label">email user</label>
                    <div class="col-sm-10">
                        <input class="form-control" type="email" placeholder="Masukkan email" name='email'
                            id="example-text-input">
                    </div>
                </div>
                <div class="form-group row">
                    <label for="example-text-input" class="col-sm-2 col-form-label">password user</label>
                    <div class="col-sm-10">
                        <input class="form-control" type="password" placeholder="Masukkan password" name='password'
                            id="example-text-input">
                    </div>
                </div>
                <div class="form-group row">
                    <label for="example-text-input" class="col-sm-2 col-form-label">Role</label>
                    <div class="col-sm-10">
                        <select class="js-example-basic-single form-control" name="role">
                            <option value="" disabled selected hidden>Pilih Role</option>
                            @foreach ($roles as $role)
                            <option value="{{ $role->nama }}">{{ $role->nama }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>

                <div class="float-right">
                    <a href="{{ route('user.index') }}" class='btn btn-light pl-md-3'>Cancel</a>
                    <button type="submit" class='btn btn-primary'>Submit</button>
                </div>
            </form>
        </div>
    </div>

</div>
@endsection
@section('script')
<script>
</script>
@endsection
