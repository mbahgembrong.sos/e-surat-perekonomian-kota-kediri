@extends('admin.layouts.app')
@section('content')
<!-- Begin Page Content -->
<div class="container-fluid">

    <div class="card shadow mb-4">
        <div class="card-header py-3">
            <h6 class="m-0 font-weight-bold text-primary">Tambah Role</h6>
        </div>
        <div class="card-body">
            <form action="{{ route('role.update',$role->id) }}" method="post" enctype="multipart/form-data">
                @csrf
                <div class="form-group row">
                    <label for="example-text-input" class="col-sm-2 col-form-label">Nama Role</label>
                    <div class="col-sm-10">
                        <input class="form-control" type="text" placeholder="Masukkan nama" name='nama'
                            id="example-text-input" value="{{ $role->nama }}">
                    </div>
                </div>
                <div class="float-right">
                    <a href="{{ route('role.index') }}" class='btn btn-light pl-md-3'>Cancel</a>
                    <button type="submit" class='btn btn-primary'>Submit</button>
                </div>
            </form>
        </div>
    </div>

</div>
@endsection
@section('script')
<script>
</script>
@endsection
