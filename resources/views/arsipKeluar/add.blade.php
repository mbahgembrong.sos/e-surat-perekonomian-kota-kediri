@extends('admin.layouts.app')
@section('content')
<!-- Begin Page Content -->
<div class="container-fluid">
    <div class="card shadow mb-4">
        <div class="card-header py-3">
            <h6 class="m-0 font-weight-bold text-primary">Tambah Surat Keluar</h6>
        </div>
        <div class="card-body">
            <form action="{{ route('arsipkeluar.store') }}" method="post" enctype="multipart/form-data">
                @csrf
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group row @error('pengirim') is-invalid @enderror">
                            <label for="example-text-input" class="col-sm-2 col-form-label">Pengirim</label>
                            <div class="col-sm-10">
                                <input class="form-control" type="text" placeholder="Masukkan pengirim"
                                    value="PEREKONOMIAN" name='pengirim' id="example-text-input">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="example-text-input @error('tanggal_surat') is-invalid @enderror"
                                class="col-sm-2 col-form-label">Tanggal Surat</label>
                            <div class="col-sm-10">
                                <input class="form-control" type="date" placeholder="Masukkan tanggal surat"
                                    name='tanggal_surat' id="example-text-input">
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group row @error('nomor_surat') is-invalid @enderror">
                            <label for="example-text-input" class="col-sm-2 col-form-label">Nomor Surat</label>
                            <div class="col-sm-10">
                                <input class="form-control" type="text" placeholder="Masukkan nomor surat"
                                    name='nomor_surat' id="example-text-input">
                            </div>
                        </div>
                        <div class="form-group row @error('tanggal_kirim') is-invalid @enderror">
                            <label for="example-text-input" class="col-sm-2 col-form-label">Tanggal Dikirim</label>
                            <div class="col-sm-10">
                                <input class="form-control" type="date" placeholder="Masukkan tanggal Dikirim"
                                    name='tanggal_kirim' id="example-text-input">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="example-text-input" class="col-sm-2 col-form-label">Keterangan</label>
                    <div class="col-sm-10">
                        <input class="form-control" type="text" placeholder="Masukkan keterangan" name='keterangan'
                            id="example-text-input">
                    </div>
                </div>

                <div class="form-group row @error('penerima') is-invalid @enderror">
                    <label for="example-text-input" class="col-sm-2 col-form-label">Tujuan</label>
                    <div class="col-sm-10">
                        <select class="js-example-placeholder-single form-control" name="penerima">
                            @foreach ($kantors as $kantor)
                            <option value="{{ $kantor->nama }}">{{ $kantor->nama }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="form-group row @error('perihal') is-invalid @enderror">
                    <label for="example-text-input" class="col-sm-2 col-form-label">Perihal</label>
                    <div class="col-sm-10">
                        <textarea id="perihal" name="perihal"></textarea>
                    </div>
                </div>
                <div class="form-group row @error('surat') is-invalid @enderror">
                    <label for="example-text-input" class="col-sm-2 col-form-label">Surat</label>
                    <div class="col-sm-10">
                        <input type="file" class="my-pond" id="upload" name="surat" />
                    </div>
                </div>
                <div class="float-right">
                    <a href="{{ route('arsipkeluar.index') }}" class='btn btn-light pl-md-3'>Cancel</a>
                    <button type="submit" class='btn btn-primary'>Submit</button>
                </div>
            </form>
        </div>
    </div>

</div>
@endsection
@section('script')
<script>
    tinymce.init({
        forced_root_block : false,
    selector: 'textarea#perihal'
    });
    tinymce.init({
        forced_root_block : false,
    selector: 'textarea#disposisi'
    });
    $(document).ready(function() {
    $('.js-example-placeholder-single').select2({placeholder: "Pilih penerima"});
    });
    $(function(){

    // Register the plugin
    FilePond.registerPlugin(FilePondPluginFileValidateType);
    const inputElement = document.querySelector('input[id="upload"]');

    // Create a FilePond instance
    const pond = FilePond.create(inputElement, {
        storeAsFile: true,
        acceptedFileTypes: ['application/msword',"application/vnd.openxmlformats-officedocument.wordprocessingml.document"],
        fileValidateTypeDetectType: (source, type) =>
        new Promise((resolve, reject) => {
        // Do custom type detection here and return with promise

        resolve(type);
        }),
    });

    });
</script>
@endsection
