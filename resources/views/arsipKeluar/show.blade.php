@extends('admin.layouts.app')
@section('content')
<div class="container-fluid">

    <div class="card shadow mb-4">
        <div class="card-header py-3">
            <h6 class="m-0 font-weight-bold text-primary">Detail Surat keluar</h6>
        </div>
        <div class="card-body">
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group row">
                        <label for="example-text-input" class="col-sm-2 col-form-label">Pengirim</label>
                        <div class="col-sm-10">
                            <input class="form-control" type="text" placeholder="keluarkan pengirim" name='pengirim'
                                readonly id="example-text-input" value="{{ $arsipkeluar->pengirim }}">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="example-text-input" class="col-sm-2 col-form-label">Tanggal Surat</label>
                        <div class="col-sm-10">
                            <input class="form-control" type="date" placeholder="keluarkan tanggal surat"
                                name='tanggal_surat' readonly id="example-text-input"
                                value="{{ $arsipkeluar->tgl_surat->format('Y-m-d') }}">
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group row">
                        <label for="example-text-input" class="col-sm-2 col-form-label">Nomor Surat</label>
                        <div class="col-sm-10">
                            <input class="form-control" type="text" placeholder="keluarkan nomor surat" readonly
                                name='nomor_surat' id="example-text-input" value="{{ $arsipkeluar->no_surat }}">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="example-text-input" class="col-sm-2 col-form-label">Tanggal Dikirim</label>
                        <div class="col-sm-10">
                            <input class="form-control" type="date" placeholder="keluarkan tanggal dikirim" readonly
                                name='tanggal_kirim' id="example-text-input"
                                value="{{ $arsipkeluar->tgl_kirim->format('Y-m-d') }}">
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <table class="table table-hover table-bordered table-condensed">
                    <thead>
                        <tr>
                            <td>Perihal</td>
                            <td>Tujuan</td>
                            <td>Keterangan</td>
                        </tr>
                    </thead>
                    <tr>
                        <td>
                            {{ $arsipkeluar->perihal }}
                        </td>
                        <td>
                            {{ $arsipkeluar->penerima }}
                        </td>
                        <td>
                            {{ $arsipkeluar->keterangan }}
                        </td>
                    </tr>
                </table>
            </div>
            <div class="float-right">
                <a href="{{ route('arsipkeluar.index') }}" class='btn btn-light pl-md-3'>Cancel</a>
                <a href="{{ route('arsipkeluar.edit',$arsipkeluar->id) }}" class='btn btn-primary'>Edit</a>
            </div>
        </div>
    </div>
</div>
<style>
    .pdfobject-container {
        height: 30rem;
        border: 1rem solid rgba(0, 0, 0, .1);
    }
</style>
<input type="hidden" id="filekeluar" value="{{ asset('storage/keluar/'.$arsipkeluar->file) }}">
@endsection
@section('script')
{{-- <script src="/js/pdfobject.js"></script> --}}
{{-- <script>
    var filekeluar = document.querySelector('input[id="filekeluar"]').value;
    PDFObject.embed(filekeluar, "#pdfviewer");
</script> --}}
{{--
    </div>
    <div class="box-body">
        <div class="row">
            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                <center>

            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                <table class="table text-lg-left">
                    <tr>
                        <th>ID</th>

                    </tr>
                    <tr>
                        <th>Nama role</th>
                        <td>:</td>

                    <tr>
                        <th>Deskripsi role</th>
                        <td>:</td>

                    <tr>
                        <th>Harga sewa / Hari</th>
                        <td>:</td>

                    </tr>
                    <tr>
                        <th>Terdaftar Pada</th>
                        <td>:</td>

                    </tr>
                </table>
            </div>
        </div>
    </div>
</div> --}}
@endsection
