<html>

<head>
    <title>&nbsp;</title>
    <link href="{{ asset('bppkad') }}/css/bootstrap.min.css" rel="stylesheet">
    <link href="{{ asset('bppkad') }}/font-awesome/css/font-awesome.css" rel="stylesheet">

    <link href="{{ asset('bppkad') }}/css/plugins/morris/morris-0.4.3.min.css" rel="stylesheet">
    <link href="{{ asset('bppkad') }}/css/plugins/timeline/timeline.css" rel="stylesheet">
    <link href="{{ asset('bppkad') }}/css/datepicker.css" rel="stylesheet">


    <script src="{{ asset('bppkad') }}/js/jquery-1.8.3.min.js"></script>
    <script src="{{ asset('bppkad') }}/js/bootstrap.js"></script>
    <script src="{{ asset('bppkad') }}/js/tinymce/tinymce.min.js"></script>
    <script src="{{ asset('bppkad') }}/js/highcharts/highcharts.js" type="text/javascript"></script>
    <script>
        tinymce.init({selector:'textarea'});

                    $(function(){
                        $("#tanggal1").datepicker({
                            format:'yyyy-mm-dd'
                        });

                        $("#tanggal2").datepicker({
                            format:'yyyy-mm-dd'
                        });

                        $("#tanggal").datepicker({
                            format:'yyyy-mm-dd'
                        });
                    })
    </script>
</head>

<body>

    <html>

    <head>
        <title>&nbsp;</title>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
        <style type="text/css" media="print">
            body,
            td,
            th {
                font-family: "Times New Roman", Times, serif;
                font-size: 18px;
                margin: 5px;
                white-space: inherit;
                letter-spacing: 1;
                line-height: 1;
            }
        </style>
        <table height="126" align="center" width="850">
            <tr>
                <td align="center"><img src="{{ asset('bppkad') }}/img/lambang.png" width="100%" height="100%"></td>
            </tr>

        </table>
    </head>

    <body onLoad="window.print()">
        <center><u>
                <h2> LEMBAR DISPOSISI</h2>
            </u></center>

        <p>
            <table width="850" border="2" align="center" cellpadding="0" cellspacing="0">
                <tr height="50">
                    <td width="385">
                        <div class="xx" style="padding-left:15px; float:left; width:135px;">Surat dari</div>
                        <div class="xx" style="padding-left:0px; float:left">: {{ $arsipmasuk->pengirim }}</div>
                    </td>
                    <td width="407">
                        <div class="xx" style="padding-left:15px; float:left; width:150px;">Diterima Tanggal</div>
                        <div class="xx" style="padding-left:0px; float:left">:
                            {{ $arsipmasuk->tgl_terima->format('d-m-Y') }}</div>
                    </td>
                </tr>
                <tr height="50">
                    <td>
                        <div class="xx" style="padding-left:15px; float:left; width:135px;">Tgl. Surat</div>
                        <div class="xx" style="padding-left:0px; float:left">:
                            {{ $arsipmasuk->tgl_surat->format('d-m-Y') }}</div>
                    </td>
                    {{-- <td>
                        <div class="xx" style="padding-left:15px; float:left; width:150px;">Nomor Agenda</div>
                        <div class="xx" style="padding-left:0px; float:left">: 1</div>
                    </td> --}}
                </tr>
                <tr height="50">
                    <td>
                        <div class="xx" style="padding-left:15px; float:left; width:135px;">Nomor Surat</div>
                        <div class="xx" style="padding-left:0px; float:left">: {{ $arsipmasuk->no_surat }}</div>
                    </td>
                    <td>
                        <div class="xx" style="padding-left:15px; float:left; width:135px;">Kode Surat</div>
                        <div class="xx" style="padding-left:15px; float:left">: {{ $arsipmasuk->keterangan }}</div>
                    </td>
                </tr>
                <tr height="85">
                    <td>
                        <div class="xx" style="padding-left:15px; float:left; width:65px;">Sifat </div>
                        <div class="xx" style="padding-left:70px; float:left">:
                            <input type="checkbox" width="20" height="20"
                                {{ $arsipmasuk->sifat=="Sangat Rahasia" ? 'checked':'' }} readonly> Sangat Rahasia
                            &nbsp;&nbsp;&nbsp;&nbsp;
                            <input type="checkbox" width="20" height="20"
                                {{ $arsipmasuk->sifat=="Rahasia" ? 'checked':'' }} readonly> Rahasia
        </p>&nbsp;
        <input type="checkbox" width="20" height="20" {{ $arsipmasuk->sifat=="Biasa" ? 'checked':'' }} readonly> Biasa
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        &nbsp;&nbsp;&nbsp;&nbsp;
        <input type="checkbox" width="20" height="20" {{ $arsipmasuk->sifat=="Terbuka" ? 'checked':'' }} readonly>
        Terbuka
        </div>
        </p>
        </p>
        </div>
        </td>
        <td>
            <div class="xx" style="padding-left:15px; float:left; width:150px;">Klasifikasi </div>
            <div class="xx" style="padding-left:0px; float:left">:
                <input type="checkbox" width="20" height="20"
                    {{ $arsipmasuk->klasifikasi=="Sangat Segera" ? 'checked':'' }} readonly> Sangat Segera &nbsp; &nbsp;
                <input type="checkbox" width="20" height="20" {{ $arsipmasuk->klasifikasi=="Biasa" ? 'checked':'' }}
                    readonly> Biasa </p>&nbsp;
                <input type="checkbox" width="20" height="20" {{ $arsipmasuk->klasifikasi=="Segera" ? 'checked':'' }}
                    readonly> Segera</div>
        </td>
        </tr>
        <tr height="140">
            <td colspan="2">
                <div class="xx" style="padding-left:15px; float:left; width:135px; padding-bottom:100px;">Perihal</div>
                <div class="xx" style="padding-left:0px; float:left;">:&nbsp;</div>
                {{ $arsipmasuk->perihal }}
            </td>
        </tr>
        <tr height="180">
            <td colspan="2" align="center"><u>
                    <h3>DITERUSKAN KEPADA :</h3>
                </u>
                @php
                $no=1;
                $countDetail=count($detailmasuk);
                @endphp
                <div class="row">
                    @if ($countDetail<5) <div class="col-md-6" style="text-align:left">
                        @foreach ($detailmasuk as $detail )
                        <div class="xx" style="padding-left:50px;padding-bottom:10px">
                            <img src="{{ asset('bppkad/img/'.($no++).'.png') }}" width="30" height="30">
                            {{ $detail->bidang }}
                        </div>
                        @endforeach
                        @endif
                
                @if ($countDetail>5)
                <div class="col-md-6" style="text-align:left">
                    @for ($i =0; $i <= $countDetail; $i++)
                    @if ($i<=3)
                    <div class="xx"
                        style="padding-left:50px; padding-bottom:10px ">
                        <img src="{{ asset('bppkad/img/'.($no++).'.png') }}" width="30" height="30">
                        {{ $detailmasuk[$i]->bidang }}
                </div>
                @endif
                @endfor
                </div>
                <div class="col-md-6" style="text-align:left">
                    @for ($i =4; $i < $countDetail; $i++)
                    <div class="xx" style="padding-left:50px;padding-bottom:10px">
                        <img src="{{ asset('bppkad/img/'.($no++).'.png') }}" width="30" height="30">
                        {{ $detailmasuk[$i]->bidang }}
                </div>
                @endfor
                </div>
                @endif
                </div>
            </td>
        </tr>
        </table>


        <center>
            <h2><u>ISI DISPOSISI</u></h2>
            <table width="850" height="100" align="center" cellpadding="0" cellspacing="0">
                <tr>
                    <td>
                        <div class="xx" style="padding-left:15px; padding-bottom:45px;">
                            {{ $arsipmasuk->isi }}
                        </div>
                    </td>
                </tr>
            </table>
            <table width="850" border="0" align="center" cellpadding="0" cellspacing="0">
                <tr>
                    <td>
                        <div class="xx" style="padding-left:0px; float:left; width:443px;">
                            <input type="checkbox" style="width:27px; height:27px;"
                                {{ $arsipmasuk->status=="Arsipkan" ? 'checked':'' }} readonly>&nbsp; Arsipkan<p>
                        </div>
                        <div class="xx" style="padding-left:0px; float:left; width:443px;">
                            <input type="checkbox" style="width:27px; height:27px;"
                                {{ $arsipmasuk->status=="Tindak Lanjuti" ? 'checked':'' }} readonly>&nbsp; Tindak
                            Lanjuti
                        </div>
                    </td>
                </tr>
            </table>
        </center>
        <p>&nbsp;</p>
        @if ($arsipmasuk->status=="Tindak Lanjuti")
        <table width="850px" height="60px" border="2" align="center" cellpadding="0" cellspacing="0">
            <tr>
                <td style="padding-bottom:0px; padding-left:15px;">Tindak Lanjut Nomor :</td>

            </tr>
        </table>
        @endif
    </body>

    </html>
    <!-- Modal -->
    <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">Konfirmasi</h4>
                </div>
                <div class="modal-body">
                    <input type="hidden" name="idhapus" id="idhapus">
                    <p>Apakah anda yakin ingin menghapus data ini?</p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-primary" id="konfirmasi">Hapus</button>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->

    <!-- Core Scripts - Include with every page -->
    <script src="{{ asset('bppkad') }}/js/holder.js"></script>
    <script src="{{ asset('bppkad') }}/js/bootstrap-datepicker.js"></script>
    <script src="{{ asset('bppkad') }}/js/application.js"></script>
    <script src="{{ asset('bppkad') }}/js/bootstrap.min.js"></script>
    <script src="{{ asset('bppkad') }}/js/plugins/metisMenu/jquery.metisMenu.js"></script>
    <script src="{{ asset('bppkad') }}/js/plugins/morris/raphael-2.1.0.min.js"></script>
    <script src="{{ asset('bppkad') }}/js/plugins/morris/morris.js"></script>
    <script src="{{ asset('bppkad') }}/js/sb-admin.js"></script>
    <script src="{{ asset('bppkad') }}/js/demo/dashboard-demo.js"></script>
</body>

</html>

