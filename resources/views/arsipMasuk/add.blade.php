@extends('admin.layouts.app')
@section('content')
<!-- Begin Page Content -->
<div class="container-fluid">

    <div class="card shadow mb-4">
        <div class="card-header py-3">
            <h6 class="m-0 font-weight-bold text-primary">Tambah Surat Masuk</h6>
        </div>
        <div class="card-body">
            <form action="{{ route('arsipmasuk.store') }}" method="post" enctype="multipart/form-data">
                @csrf
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group row">
                            <label for="example-text-input" class="col-sm-2 col-form-label">Pengirim</label>
                            <div class="col-sm-10">
                                <input class="form-control" type="text" placeholder="Masukkan pengirim" name='pengirim'
                                    id="example-text-input">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="example-text-input" class="col-sm-2 col-form-label">Tanggal Surat</label>
                            <div class="col-sm-10">
                                <input class="form-control" type="date" placeholder="Masukkan tanggal surat"
                                    name='tanggal_surat' id="example-text-input">
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group row">
                            <label for="example-text-input" class="col-sm-2 col-form-label">Nomor Surat</label>
                            <div class="col-sm-10">
                                <input class="form-control" type="text" placeholder="Masukkan nomor surat"
                                    name='nomor_surat' id="example-text-input">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="example-text-input" class="col-sm-2 col-form-label">Tanggal Diterima</label>
                            <div class="col-sm-10">
                                <input class="form-control" type="date" placeholder="Masukkan tanggal diterima"
                                    name='tanggal_terima' id="example-text-input">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="example-text-input" class="col-sm-2 col-form-label">Keterangan</label>
                    <div class="col-sm-10">
                        <input class="form-control" type="text" placeholder="Masukkan keterangan" name='keterangan'
                            id="example-text-input">
                    </div>
                </div>
                <div class="form-group row">
                    <label for="example-text-input" class="col-sm-2 col-form-label">Sifat</label>
                    <div class="col-sm-10">
                        <div class="form-check form-check-inline">
                            <input class="form-check-input" type="radio" name="sifat" value="Sangat Rahasia"
                                id="Sifat1">
                            <label class="form-check-label" for="Sifat1">
                                Sangat Rahasia
                            </label>
                        </div>
                        <div class="form-check form-check-inline">
                            <input class="form-check-input" type="radio" name="sifat" value="Rahasia" id="Sifat2">
                            <label class="form-check-label" for="Sifat2">
                                Rahasia
                            </label>
                        </div>
                        <div class="form-check form-check-inline">
                            <input class="form-check-input" type="radio" name="sifat" value="Biasa" id="Sifat3">
                            <label class="form-check-label" for="Sifat3">
                                Biasa
                            </label>
                        </div>
                        <div class="form-check form-check-inline">
                            <input class="form-check-input" type="radio" name="sifat" value="Terbuka" id="Sifat4">
                            <label class="form-check-label" for="Sifat4">
                                Terbuka
                            </label>
                        </div>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="example-text-input" class="col-sm-2 col-form-label">Klasifikasi</label>
                    <div class="col-sm-10">
                        <div class="form-check form-check-inline">
                            <input class="form-check-input" type="radio" name="klasifikasi" value="Sangat Segera"
                                id="klasifikasi1">
                            <label class="form-check-label" for="klasifikasi1">
                                Sangat Segera
                            </label>
                        </div>
                        <div class="form-check form-check-inline">
                            <input class="form-check-input" type="radio" name="klasifikasi" value="Segera"
                                id="klasifikasi2">
                            <label class="form-check-label" for="klasifikasi2">
                                Segera
                            </label>
                        </div>
                        <div class="form-check form-check-inline">
                            <input class="form-check-input" type="radio" name="klasifikasi" value="Biasa"
                                id="klasifikasi3">
                            <label class="form-check-label" for="klasifikasi3">
                                Biasa
                            </label>
                        </div>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="example-text-input" class="col-sm-2 col-form-label">Perihal</label>
                    <div class="col-sm-10">
                        <textarea id="perihal" name="perihal"></textarea>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="example-text-input" class="col-sm-2 col-form-label">Kepada</label>
                    <div class="col-sm-10">
                        <select class="js-example-placeholder-multiple form-control" name="kepada[]"
                            multiple="multiple">
                            @foreach ($bidangs as $bidang)
                            <option value="{{ $bidang->nama }}">{{ $bidang->nama }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="example-text-input" class="col-sm-2 col-form-label">disposisi</label>
                    <div class="col-sm-10">
                        <textarea id="disposisi" name="disposisi"></textarea>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="example-text-input" class="col-sm-2 col-form-label">Surat</label>
                    <div class="col-sm-10">
                        <input type="file" class="my-pond" id="upload" name="surat" />
                    </div>
                </div>
                <div class="form-group row">
                    <label for="example-text-input" class="col-sm-2 col-form-label">Status</label>
                    <div class="col-sm-10">
                        <div class="form-check form-check-inline">
                            <input class="form-check-input" type="radio" name="status" value="Arsipkan" id="status1">
                            <label class="form-check-label" for="status1">
                                Arsipkan
                            </label>
                        </div>
                        <div class="form-check form-check-inline">
                            <input class="form-check-input" type="radio" name="status" value="Tindak Lanjuti"
                                id="status2">
                            <label class="form-check-label" for="status2">
                                Tindak Lanjuti
                            </label>
                        </div>
                    </div>
                </div>

                <div class="float-right">
                    <a href="{{ route('arsipmasuk.index') }}" class='btn btn-light pl-md-3'>Cancel</a>
                    <button type="submit" class='btn btn-primary'>Submit</button>
                </div>
            </form>
        </div>
    </div>

</div>
@endsection
@section('script')
<script>
    tinymce.init({
        forced_root_block : false,
    selector: 'textarea#perihal'
    });
    tinymce.init({
        forced_root_block : false,
    selector: 'textarea#disposisi'
    });
    $(document).ready(function() {
    $('.js-example-placeholder-multiple').select2({placeholder: "Pilih penerima"});
    });
    $(function(){

    // Register the plugin
    FilePond.registerPlugin(FilePondPluginFileValidateType);
    const inputElement = document.querySelector('input[id="upload"]');

    // Create a FilePond instance
    const pond = FilePond.create(inputElement, {
        storeAsFile: true,
        acceptedFileTypes: ['application/pdf','image/*','application/msword',,"application/vnd.openxmlformats-officedocument.wordprocessingml.document"],
        fileValidateTypeDetectType: (source, type) =>
        new Promise((resolve, reject) => {
        // Do custom type detection here and return with promise

        resolve(type);
        }),
    });

    });
</script>
@endsection
