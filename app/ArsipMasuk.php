<?php

namespace App;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Ramsey\Uuid\Exception\UnsatisfiedDependencyException;
use Ramsey\Uuid\Uuid as Generator;

class ArsipMasuk extends Model
{
    // use HasFactory;
    protected $table = 'arsip_masuk';

    protected $primaryKey = 'id';
    public $incrementing = false;
    protected $dates = ['tgl_terima','tgl_surat'];

    protected $fillable = ['id','pengirim','tgl_surat','tgl_terima','no_surat','sifat','klasifikasi','keterangan','perihal','isi','file','status','bidang'];
    public function detail(){
        return $this -> hashMany(DetailMasuk::class);
    }
    protected static function boot()
    {
        parent::boot();

        static::creating(function ($model) {
            try {
                $model->id = Generator::uuid4()->toString();
            } catch (UnsatisfiedDependencyException $e) {
                abort(500, $e->getMessage());
            }
        });
    }
}
