<?php

namespace App;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Role extends Model
{
    // use HasFactory;
    protected $table = 'role';

    protected $primaryKey = 'id';
    public $incrementing = false;
    protected $fillable = ['nama'];
}
