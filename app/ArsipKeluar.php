<?php

namespace App;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Ramsey\Uuid\Exception\UnsatisfiedDependencyException;
use Ramsey\Uuid\Uuid as Generator;

class ArsipKeluar extends Model
{
    // use HasFactory;
    protected $table = 'arsip_keluar';

    protected $primaryKey = 'id';
    public $incrementing = false;
    protected $dates = ['tgl_kirim','tgl_surat'];

    protected $fillable = ['id','pengirim','penerima','tgl_surat','tgl_kirim','no_surat','keterangan','perihal','file','bidang'];
     protected static function boot()
    {
        parent::boot();

        static::creating(function ($model) {
            try {
                $model->id = Generator::uuid4()->toString();
            } catch (UnsatisfiedDependencyException $e) {
                abort(500, $e->getMessage());
            }
        });
    }

}
