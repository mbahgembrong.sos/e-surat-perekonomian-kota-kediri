<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
         $year = DB::select(DB::raw('select year(now()) as year'))[0]
                    ->year;
        $month = DB::select(DB::raw('select month(now()) as month'))[0]
                    ->month;
        $day = DB::select(DB::raw('select day(now()) as day'))[0]
                    ->day;
                    if (Auth::user()->nama_role == "admin") {
                        $masukDay = DB::table('arsip_masuk')
                                            ->whereYear('updated_at',$year)
                                            ->WhereMonth('updated_at',$month)
                                            ->WhereDay('updated_at',$day)
                                            ->count();
                        $keluarDay = DB::table('arsip_keluar')
                                            ->whereYear('updated_at',$year)
                                            ->WhereMonth('updated_at',$month)
                                            ->WhereDay('updated_at',$day)
                                            ->count();
                        $totalMasuk = DB::table('arsip_masuk')
                                            ->count();
                        $totalKeluar = DB::table('arsip_keluar')
                                            ->count();

                    $months=["January","February"
                                ,"March","April"
                                ,"May","June"
                                ,"July","August"
                                ,"September","October"
                                ,"November","December"];
                    $dataMasuk = [];
                    for ($i=0; $i < count($months); $i++)
                    {
                        $this->month = $i+1;
                            $perMonth = DB::table('arsip_masuk')
                                                ->WhereYear('created_at',$year)
                                                ->WhereMonth('created_at',$this->month)
                                                ->count();
                        // }
                        array_push($dataMasuk,["month"=>$months[$i],"total"=> $perMonth]);
                    }
                    $dataKeluar = [];
                    for ($i=0; $i < count($months); $i++)
                    {
                        $this->month = $i+1;
                            $perMonth = DB::table('arsip_keluar')
                                                ->WhereYear('created_at',$year)
                                                ->WhereMonth('created_at',$this->month)
                                                ->count();
                        // }
                        array_push($dataKeluar,["month"=>$months[$i],"total"=> $perMonth]);
                    }


                    return view('home',['masukDay'=>$masukDay,'keluarDay'=>$keluarDay,'totalMasuk'=>$totalMasuk,'totalKeluar'=>$totalKeluar,'dataMasuk'=>json_encode($dataMasuk,JSON_PRETTY_PRINT),'dataKeluar'=>json_encode($dataKeluar,JSON_PRETTY_PRINT)]);
                    }
                    else{
                        $masukDay = DB::table('arsip_masuk')
                        ->where('bidang','LIKE',Auth::user()->nama_role)->orwhere('bidang','LIKE',"all")
                                            ->whereYear('updated_at',$year)
                                            ->WhereMonth('updated_at',$month)
                                            ->WhereDay('updated_at',$day)
                                            ->count();
                        $keluarDay = DB::table('arsip_keluar')->where('bidang','LIKE',Auth::user()->nama_role)->orwhere('bidang','LIKE',"all")
                                            ->whereYear('updated_at',$year)
                                            ->WhereMonth('updated_at',$month)
                                            ->WhereDay('updated_at',$day)
                                            ->count();
                        $totalMasuk = DB::table('arsip_masuk')
                        ->where('bidang','LIKE',Auth::user()->nama_role)->orwhere('bidang','LIKE',"all")
                                            ->count();
                        $totalKeluar = DB::table('arsip_keluar')
                        ->where('bidang','LIKE',Auth::user()->nama_role)->orwhere('bidang','LIKE',"all")
                                            ->count();

                    $months=["January","February"
                                ,"March","April"
                                ,"May","June"
                                ,"July","August"
                                ,"September","October"
                                ,"November","December"];
                    $dataMasuk = [];
                    for ($i=0; $i < count($months); $i++)
                    {
                        $this->month = $i+1;
                            $perMonth = DB::table('arsip_masuk')
                            ->where('bidang','LIKE',Auth::user()->nama_role)
                                                ->orwhere('bidang','LIKE',"all")
                                                ->WhereYear('created_at',$year)
                                                ->WhereMonth('created_at',$this->month)
                                                ->count();
                        // }
                        array_push($dataMasuk,["month"=>$months[$i],"total"=> $perMonth]);
                    }
                    $dataKeluar = [];
                    for ($i=0; $i < count($months); $i++)
                    {
                        $this->month = $i+1;
                            $perMonth = DB::table('arsip_keluar')
                            ->where('bidang','LIKE',Auth::user()->nama_role)
                                                ->orwhere('bidang','LIKE',"all")
                                                ->WhereYear('created_at',$year)
                                                ->WhereMonth('created_at',$this->month)
                                                ->count();
                        // }
                        array_push($dataKeluar,["month"=>$months[$i],"total"=> $perMonth]);
                    }


                     return view('home',['masukDay'=>$masukDay,'keluarDay'=>$keluarDay,'totalMasuk'=>$totalMasuk,'totalKeluar'=>$totalKeluar,'dataMasuk'=>json_encode($dataMasuk,JSON_PRETTY_PRINT),'dataKeluar'=>json_encode($dataKeluar,JSON_PRETTY_PRINT)]);
                    }
    }
}
