<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use App\Role;
use Illuminate\Support\Facades\Hash;
use Alert;
class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = user::get();
        return view('user.index', compact('user'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $roles = Role::all();
        return view('user.add',compact('roles'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            $request->validate(
            [
                'nama' => 'required|min:3',
                'role' => 'required'
            ],
            [
                'nama.required' => 'Nama user Harus diisi',
                'role.required' => 'Nama Role Harus diisi',
                'nama.min' => 'Nama user Minimal 3 Karakter'
            ]
        );

        $user = new user;
        $user->nama = $request->nama;
        $user->email = $request->email;
        $user->password = Hash::make($request->password);
        $user->nama_role = $request->role;
        $user->save();
         Alert::success('Tambah User', 'Data berhasil disimpan');
        return redirect()->route('user.index');
        } catch (\Throwable $th) {
             Alert::warning('Tambah User', 'Gagal Tambah Data.');
                return redirect()->back();
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\user  $user
     * @return \Illuminate\Http\Response
     */
    public function show($user)
    {
        // var_dump($user);
        $user = user::findOrfail($user);
        return view('user.show', compact('user'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\user  $user
     * @return \Illuminate\Http\Response
     */
    public function edit( $user)
    {
        $user = user::findOrfail($user);
        $roles = Role::all();
        return view('user.edit' ,compact(['user','roles']));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\user  $user
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request,$user)
    {
        try {
            $request->validate(
            [
                'nama' => 'required|min:3',
                 'role' => 'required',
            ],
            [
                'nama.required' => 'Nama user Harus diisi',
                'nama.min' => 'Nama user Minimal 3 Karakter',
                'role.required' => 'Nama Role Harus diisi',
            ]
        );
        $user = user::findOrFail($user);
        $user->nama = $request->nama;
        $user->email = $request->email;
        if (isset($request->password)) {
            $user->password = Hash::make($request->password);
        }
        $user->nama_role = $request->role;
        $user->update();
        Alert::success('Update User', 'Data berhasil dirubah');
        return redirect()->route('user.index')->with('message', 'Data berhasil diupdate');
        } catch (\Throwable $th) {
            Alert::warning('Update User', 'Gagal Rubah Data.');
        return redirect()->back();
        }

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\user  $user
     * @return \Illuminate\Http\Response
     */
    public function destroy($user)
    {
        try {
            $user = user::findOrFail($user);
        $user->delete();
        Alert::success('Delete User', 'Data berhasil dihapus');
        return redirect()->back();
        } catch (\Throwable $th) {
             Alert::warning('Delete User', 'Gagal Hapus Data.');
            return redirect()->back();
        }

    }
}
