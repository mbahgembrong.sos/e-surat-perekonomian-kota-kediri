<?php

namespace App\Http\Controllers;

use App\Role;
use Illuminate\Http\Request;
use Haruncpi\LaravelIdGenerator\IdGenerator;
use Alert;

class RoleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $role = Role::get();
        return view('role.index', compact('role'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('role.add');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            $id = IdGenerator::generate(['table' => 'role', 'field'=>'id', 'length' => 10, 'prefix' =>'ROLE-']);

        $request->validate(
            [
                'nama' => 'required|min:3',
            ],
            [
                'nama.required' => 'Nama Role Harus diisi',

                'nama.min' => 'Nama Role Minimal 3 Karakter'
            ]
        );

        $role = new Role;
        $role->id = $id;
        $role->nama = $request->nama;
        $role->save();
        Alert::success('Tambah Role', 'Data berhasil disimpan');
        return redirect()->route('role.index');
        } catch (\Throwable $th) {
            Alert::warning('Tambah role', 'Gagal Tambah Data.');
                return redirect()->back();
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Role  $role
     * @return \Illuminate\Http\Response
     */
    public function show(Role $role)
    {
        $role = Role::findOrfail($role);
        return view('role.detail', compact('role'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Role  $role
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $role = Role::findOrfail($id);
        return view('role.edit', compact('role'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Role  $role
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try {
             $request->validate(
            [
                'nama' => 'required|min:3',

            ],
            [
                'nama.required' => 'Nama Role Harus diisi',
                'nama.min' => 'Nama Role Minimal 3 Karakter',

            ]
        );

        $role = Role::findOrfail($id);
        $role->nama = $request->nama;
        $role->update();
        Alert::success('Update Role', 'Data berhasil dirubah');
        return redirect()->route('role.index');
        } catch (\Throwable $th) {
             Alert::warning('Update Role', 'Gagal Rubah Data.');
        return redirect()->back();
        }

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Role  $role
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
             $role = Role::findOrfail($id);
        $role->delete();
        Alert::success('Delete Role', 'Data berhasil dihapus');
        return redirect()->back();
        } catch (\Throwable $th) {
             Alert::warning('Delete Kantor', 'Gagal Hapus Data.');
            return redirect()->back();
        }

    }
}
