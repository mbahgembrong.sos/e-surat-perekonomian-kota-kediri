<?php

namespace App\Http\Controllers;

use App\Kantor;
use Illuminate\Http\Request;
use Haruncpi\LaravelIdGenerator\IdGenerator;
use Alert;

class KantorController extends Controller
{
     /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $kantor = Kantor::get();
        return view('kantor.index', compact('kantor'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('kantor.add');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
             $id = IdGenerator::generate(['table' => 'kantor', 'field'=>'id', 'length' => 10, 'prefix' =>'KTR-']);

        $request->validate(
            [
                'nama' => 'required|min:3',
                'alamat' => 'required|min:3',
            ],
            [
                'nama.required' => 'Nama Kantor Harus diisi',
                'alamat.required' => 'Alamat Kantor Harus diisi',
                'nama.min' => 'Nama Kantor Minimal 3 Karakter'
            ]
        );

        $kantor = new Kantor;
        $kantor->id = $id;
        $kantor->nama = $request->nama;
        $kantor->alamat = $request->alamat;
        $kantor->save();
        Alert::success('Tambah kantor', 'Data berhasil disimpan');
        return redirect()->route('kantor.index');
        } catch (\Throwable $th) {
            Alert::warning('Tambah Bidang', 'Gagal Tambah Data.');
                return redirect()->back();
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Kantor  $kantor
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $kantor = Kantor::findOrfail($id);
        return view('kantor.detail', compact('kantor'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Kantor  $kantor
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $kantor = Kantor::findOrfail($id);
        return view('kantor.edit', compact('kantor'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Kantor  $kantor
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request,$id)
    {
        try {
            $request->validate(
            [
                'nama' => 'required|min:3',
            ],
            [
                'nama.required' => 'Nama Kantor Harus diisi',
                'nama.min' => 'Nama Kantor Minimal 3 Karakter',
            ]
        );

        $kantor = Kantor::findOrFail($id);
        $kantor->nama = $request->nama;
        $kantor->alamat = $request->alamat;
        $kantor->update();
        Alert::success('Update Kantor', 'Data berhasil dirubah');
        return redirect()->route('kantor.index');
        } catch (\Throwable $th) {
            Alert::warning('Update Kantor', 'Gagal Rubah Data.');
        return redirect()->back();
        }

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Kantor  $kantor
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            $kantor = Kantor::findOrFail($id);
        $kantor->delete();
        Alert::success('Delete Kantor', 'Data berhasil dihapus');
        return redirect()->back();
        } catch (\Throwable $th) {
            Alert::warning('Delete Kantor', 'Gagal Hapus Data.');
            return redirect()->back();
        }

    }
}
