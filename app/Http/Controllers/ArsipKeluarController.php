<?php

namespace App\Http\Controllers;

use App\ArsipKeluar;
use Illuminate\Http\Request;
use App\kantor;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;
use PDF;
use Alert;
use SimpleSoftwareIO\QrCode\Facades\QrCode;

class ArsipKeluarController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $arsipkeluar = arsipkeluar::get();
        if (!extension_loaded('imagick')) {
            Alert::warning('IMAGICK NOT INSTALED', 'extension php imagick belum di instal');
        }
        return view('arsipkeluar.index', compact('arsipkeluar'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $kantors = kantor::all();
        return view('arsipkeluar.add', compact(['kantors']));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            $request->validate(
                [
                    'pengirim' => 'required',
                    'penerima' => 'required',
                    'tanggal_surat' => 'required',
                    'nomor_surat' => 'required',
                    'tanggal_kirim' => 'required',
                    'perihal' => 'required',
                    'surat' => 'required',
                ],
                [
                    'pengirim.required' => 'Pengirim  Harus diisi',
                    'penerima.required' => 'penerima  Harus diisi',
                    'tanggal_surat.required' => 'Tanggal Surat  Harus diisi',
                    'nomor_surat.required' => 'Nomor Surat  Harus diisi',
                    'tanggal_kirim.required' => 'Tanggal kirim  Harus diisi',
                    'perihal.required' => 'Perihal Harus diisi',
                    'surat.required' => 'File Surat Harus diisi',
                ]
            );

            if ($request->hasFile('surat')) {
                $filenameWithExt = $request->file('surat')->getClientOriginalName();
                //Get just filename
                $filename = pathinfo($filenameWithExt, PATHINFO_FILENAME);
                // Get just ext
                $extension = $request->file('surat')->getClientOriginalExtension();
                // Filename to store
                $fileNameToStore = $filename . '_' . time() . '.' . $extension;
                // Upload Image
                $path = $request->file('surat')->storeAs('public/keluar', $fileNameToStore);
            }

            $arsipkeluar = new arsipkeluar;
            $arsipkeluar->pengirim = $request->pengirim;
            $arsipkeluar->penerima = $request->penerima;
            $arsipkeluar->tgl_surat = $request->tanggal_surat;
            $arsipkeluar->no_surat = $request->nomor_surat;
            $arsipkeluar->tgl_kirim = $request->tanggal_kirim;
            if (isset($request->keterangan)) {
                $arsipkeluar->keterangan = $request->keterangan;
            }
            $arsipkeluar->perihal = $request->perihal;
            $arsipkeluar->file = $fileNameToStore;
            if (Auth::user()->nama_role == "admin") {
                $arsipkeluar->bidang = "all";
            } else {
                $arsipkeluar->kantor =  Auth::user()->nama_role;
            }
            $arsipkeluar->save();
            ALert::success('Tambah Arsip Keluar', 'Data berhasil disimpan');
            return redirect()->route('arsipkeluar.index');
        } catch (\Exception $e) {
            Alert::warning('Tambah Arsip Keluar', 'Gagal Tambah Data.');
            return redirect()->back();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\arsipkeluar  $arsipkeluar
     * @return \Illuminate\Http\Response
     */
    public function show($arsipkeluar)
    {
        $arsipkeluar = arsipkeluar::findOrfail($arsipkeluar);
        $kantors = kantor::all();
        return view('arsipkeluar.show', compact(['arsipkeluar', 'kantors']));
    }
    public function cetakPDF($arsipkeluar)
    {
        $arsipkeluar = arsipkeluar::findOrfail($arsipkeluar);
        return view('cetak.keluarPDF', compact('arsipkeluar'));
        // $pdf = PDF::loadview('cetak.keluarPDF',['arsipkeluar'=>$arsipkeluar=>$detailkeluar]);
        // return $pdf->download('lembar-disposisi-arsipkeluar.pdf');
    }
    public function download($arsipkeluar)
    {
        $arsipkeluar = arsipkeluar::findOrfail($arsipkeluar);
        $filepath = public_path() . "/storage/keluar/$arsipkeluar->file";
        $templateProcessor = new \PhpOffice\PhpWord\TemplateProcessor($filepath);
        $qrCode = QrCode::format('png')->margin(2)->size(500)->generate(env('APP_URL') . '/scan?qrcode=' . $arsipkeluar->id);
        Storage::put('QRCode/' . $arsipkeluar->id . '.png', $qrCode);
        $templateProcessor->setImageValue('ttd',  array('path' => Storage::path('QRCode/' . $arsipkeluar->id . '.png'), 'width' => 250, 'height' => 250, 'ratio' => false));
        $templateProcessor->saveAs($filepath);
        return response()->download($filepath);
    }
    public function scan(Request $request)
    {
        $id = $request->get('qrcode');
        try {
            $arsipkeluar = arsipkeluar::findOrfail($id);
            return response([
                'status' => true,
                'message' => 'Tanda tangan asli',
            ], 200);
        } catch (\Throwable $th) {
            return response([
                'status' => true,
                'message' => 'Tanda tangan palsu',
            ], 404);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\arsipkeluar  $arsipkeluar
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $arsipkeluar = arsipkeluar::findOrfail($id);
        $kantors = kantor::all();
        // dd($detailkeluar);
        return view('arsipkeluar.edit', compact(['arsipkeluar', 'kantors']));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\arsipkeluar  $arsipkeluar
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try {
            $request->validate(
                [
                    'pengirim' => 'required',
                    'penerima' => 'required',
                    'tanggal_surat' => 'required',
                    'nomor_surat' => 'required',
                    'tanggal_kirim' => 'required',
                    'perihal' => 'required',
                ],
                [
                    'pengirim.required' => 'Pengirim  Harus diisi',
                    'penerima.required' => 'penerima  Harus diisi',
                    'tanggal_surat.required' => 'Tanggal Surat  Harus diisi',
                    'nomor_surat.required' => 'Nomor Surat  Harus diisi',
                    'tanggal_kirim.required' => 'Tanggal kirim  Harus diisi',
                    'perihal.required' => 'Perihal Harus diisi',
                ]
            );

            $arsipkeluar = arsipkeluar::findOrfail($id);
            $arsipkeluar->pengirim = $request->pengirim;
            $arsipkeluar->penerima = $request->penerima;
            $arsipkeluar->tgl_surat = $request->tanggal_surat;
            $arsipkeluar->no_surat = $request->nomor_surat;
            $arsipkeluar->tgl_kirim = $request->tanggal_kirim;
            if (isset($request->keterangan)) {
                $arsipkeluar->keterangan = $request->keterangan;
            }
            $arsipkeluar->perihal = $request->perihal;

            if (Auth::user()->nama_role == "admin") {
                $arsipkeluar->bidang = "all";
            } else {
                $arsipkeluar->kantor =  Auth::user()->nama_role;
            }
            $arsipkeluar->update();
            Alert::success('Update Arsip Keluar', 'Data berhasil dirubah');
            return redirect()->route('arsipkeluar.index');
        } catch (\Exception $e) {
            Alert::warning('Update Arsip Keluar', 'Gagal Rubah Data.');
            return redirect()->back();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\arsipkeluar  $arsipkeluar
     * @return \Illuminate\Http\Response
     */
    public function destroy($arsipkeluar)
    {
        try {
            $arsipkeluar = arsipkeluar::findOrfail($arsipkeluar);
            if (is_file($arsipkeluar->file)) {
                Storage::delete($arsipkeluar->file);
                Storage::delete('public/keluar/' . $arsipkeluar->file);
                unlink(storage_path("public/keluar/$arsipkeluar->file"));
            }
            $arsipkeluar->delete();
            Alert::success('Delete Arsip Keluar', 'Data berhasil dihapus');
            return redirect()->back();
        } catch (\Throwable $th) {
            Alert::warning('Delete Arsip Keluar', 'Gagal Hapus Data.');
            return redirect()->back();
        }
    }
}
