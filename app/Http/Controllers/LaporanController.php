<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\ArsipMasuk;
use App\ArsipKeluar;
use App\Role;
use Illuminate\Support\Facades\DB;
use App\Exports\ArsipMasukExport;
use App\Exports\ArsipKeluarExport;
use Maatwebsite\Excel\Facades\Excel;
use PDF;

class LaporanController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function arsipmasuk(Request $request)
    {
        $tanggal_awal = $request->get('tanggal_awal');
        $tanggal_selesai = $request->get('tanggal_selesai');
        $status = $request->get('status');
        if (empty($tanggal_awal)) {
            $arsipMasuk =ArsipMasuk::all();
        }else{
            if ($status == 'Semua') {
                $arsipMasuk = DB::select('select * from arsip_masuk where (tgl_terima BETWEEN :tgl_mulai AND :tgl_selesai)', [':tgl_mulai' => "$tanggal_awal", ':tgl_selesai' => $tanggal_selesai,]);
            }else {
                $arsipMasuk = DB::select("select * from arsip_masuk where (tgl_terima BETWEEN :tgl_mulai AND :tgl_selesai) AND status LIKE :status", [':tgl_mulai' => "$tanggal_awal", ':tgl_selesai' => $tanggal_selesai,':status'=> $status]);
            }
        }

        return view('laporan.arsipMasuk', compact('arsipMasuk'));
    }
    public function arsipmasuk_excel(Request $request)
    {
        // $tanggal_awal = $request->get('tanggal_awal');
        // $tanggal_selesai = $request->get('tanggal_selesai');
        // $status = $request->get('status');
        // if (empty($tanggal_awal)) {
        //     $arsipMasuk =ArsipMasuk::all();
        // }else{
        //     if ($status == 'Semua') {
        //         $arsipMasuk = DB::select('select * from arsip_masuk where (tgl_terima BETWEEN :tgl_mulai AND :tgl_selesai)', [':tgl_mulai' => "$tanggal_awal", ':tgl_selesai' => $tanggal_selesai,]);
        //     }else {
        //         $arsipMasuk = DB::select("select * from arsip_masuk where (tgl_terima BETWEEN :tgl_mulai AND :tgl_selesai) AND status LIKE :status", [':tgl_mulai' => "$tanggal_awal", ':tgl_selesai' => $tanggal_selesai,':status'=> $status]);
        //     }
        // }

        return Excel::download(new ArsipMasukExport, 'arsipmasuk.xlsx');
    }
    public function arsipmasuk_pdf(Request $request)
    {
        $arsipmasuks = ArsipMasuk::all();

    	$pdf = PDF::loadview('cetak.masukExcel',['arsipmasuks'=>$arsipmasuks]);
    	return $pdf->download('laporan-arsipmasuk.pdf');
    }


    public function arsipkeluar(Request $request)
    {
        $tanggal_awal = $request->get('tanggal_awal');
        $tanggal_selesai = $request->get('tanggal_selesai');
        $bidang = $request->get('role');
        if (empty($tanggal_awal)) {
            $arsipkeluar =Arsipkeluar::all();
        }else{
            if ($bidang == '') {
                $arsipkeluar = DB::select('select * from arsip_keluar where (tgl_kirim BETWEEN :tgl_mulai AND :tgl_selesai)', [':tgl_mulai' => "$tanggal_awal", ':tgl_selesai' => $tanggal_selesai,]);
            }else {
                $arsipkeluar = DB::select("select * from arsip_keluar where (tgl_kirim BETWEEN :tgl_mulai AND :tgl_selesai) AND bidang LIKE :bidang", [':tgl_mulai' => "$tanggal_awal", ':tgl_selesai' => $tanggal_selesai,':bidang'=> $bidang]);
            }
        }
        $roles=Role::all();

        return view('laporan.arsipkeluar', compact('arsipkeluar','roles'));
    }
    public function arsipkeluar_excel(Request $request)
    {
        // $tanggal_awal = $request->get('tanggal_awal');
        // $tanggal_selesai = $request->get('tanggal_selesai');
        // $bidang = $request->get('bidang');
        // if (empty($tanggal_awal)) {
        //     $arsipkeluar =Arsipkeluar::all();
        // }else{
        //     if ($bidang == 'Semua') {
        //         $arsipkeluar = DB::select('select * from arsip_keluar where (tgl_kirim BETWEEN :tgl_mulai AND :tgl_selesai)', [':tgl_mulai' => "$tanggal_awal", ':tgl_selesai' => $tanggal_selesai,]);
        //     }else {
        //         $arsipkeluar = DB::select("select * from arsip_keluar where (tgl_kirim BETWEEN :tgl_mulai AND :tgl_selesai) AND bidang LIKE :bidang", [':tgl_mulai' => "$tanggal_awal", ':tgl_selesai' => $tanggal_selesai,':bidang'=> $bidang]);
        //     }
        // }

        return Excel::download(new ArsipKeluarExport, 'arsipkeluar.xlsx');
    }
    public function arsipkeluar_pdf(Request $request)
    {
        $arsipkeluars = Arsipkeluar::all();

    	$pdf = PDF::loadview('cetak.keluarExcel',['arsipkeluars'=>$arsipkeluars]);
    	return $pdf->download('laporan-arsipkeluar.pdf');
    }



}
